//
//  MessagesViewModel.swift
//  RestFireChat
//
//  Created by Yuri Che on 9/4/18.
//  Copyright © 2018 Yuri Che. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Bond
import ReactiveKit

class MessagesViewModel {
    let messagesService: MessagesServiceProtocol
    let messagesArray = MutableObservableArray<Message>([])
    
    init(msgService: MessagesServiceProtocol = MessagesService() ) {
        self.messagesService = msgService
        NotificationCenter.default.addObserver(self, selector: #selector(willResignActive), name: .UIApplicationWillResignActive, object: nil)
    }
    
    @objc func willResignActive(_ notification: Notification) {
        if(!messagesArray.isEmpty){
        LocalStorageManager.saveToUserdefaults(allMessages: self.messagesArray.array)
        print("saving all messages locally")
        }
    }
    
    func getLocalMessages(){
        print("getting local messages")
        if LocalStorageManager.readSaved() != nil {
            messagesArray.insert(contentsOf: LocalStorageManager.readSaved()!, at: messagesArray.array.count)
        }
    }
    
    func getMessages()  {
        messagesService.fetchMessages { (messages, error) in
            if error != nil{
                print("error recieved")
            }
            else{
                var tempArray = [Message]()
                for msg in messages! {
                    msg.isNewMessage = self.isNewMessageInAllStorages(message: msg)
                    if(self.isNewMessageInAllStorages(message: msg)){
                        tempArray.append(msg)
                    }
                }
                
                if(!tempArray.isEmpty){
                    let sortedArray = tempArray.sorted(by: { ( messageOne , messageTwo) -> Bool in
                        Timestamp.convertFromTimestamp(seconds: messageOne.timestamp).compare(Timestamp.convertFromTimestamp(seconds: messageTwo.timestamp) as Date) == .orderedAscending
                        
                    })
                    self.messagesArray.insert(contentsOf: sortedArray, at: self.messagesArray.count)
                }
            }
            print("view model got \(self.messagesArray.count)  messages")
        }
    }
    
    func isNewMessageInAllStorages(message: Message) -> Bool {
        var localStorage = [Message]()
        
        if(LocalStorageManager.readSaved() != nil &&
            !(LocalStorageManager.readSaved()?.isEmpty)! ) {
            localStorage = LocalStorageManager.readSaved()!
        }
        let allMessagesArray  =  Set(self.messagesArray.array + localStorage)
        let ifContain = !allMessagesArray.contains(message)
        print("message with text: \(message.messageText) in all storages is \(ifContain)")
        return ifContain
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        print("viewModel deinit fired")
    }
}
