//
//  ComposeViewModel.swift
//  RestFireChat
//
//  Created by Yuri Che on 9/6/18.
//  Copyright © 2018 Yuri Che. All rights reserved.
//

import Foundation

class ComposeViewModel {
    
  let messagesService: MessagesServiceProtocol
  init(msgService: MessagesServiceProtocol = MessagesService() ) {
        self.messagesService = msgService
    }
    
    
    func postMessage(messageText: String, complete: @escaping ( _ messageID: String?)->())  {
        let messg = Message(text: messageText)
        messg.timestamp =  Timestamp.getCurrentTimestamp()
        messg.username = UUID().uuidString
        messagesService.sendMessage(message: messg) { ( messageID , finished ) in
            if( messageID != nil && finished == true ){
                complete(messageID)
            }
            else{
                 complete(nil)
            }
        }
    }
    
}

