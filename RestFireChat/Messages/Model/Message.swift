//
//  Message.swift
//  RestFireChat
//
//  Created by Yuri Che on 9/4/18.
//  Copyright © 2018 Yuri Che. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire_SwiftyJSON
import Repeat
import Bond
import ReactiveKit

class Message: NSObject, Codable{
    
    var timer: Repeater?
    var cellState = Observable<Int>(0)
    
    var isNewMessage: Bool {
        willSet{
            if(newValue == true){
                print("message is new and text is red. timer started")
                self.cellState.value = 2
                self.timer = Repeater.every(.seconds(10.0), count: 2, tolerance: .nanoseconds(0), queue: nil, { _ in
                    self.cellState.value -= 1
                    if (self.timer?.remainingIterations == 0){
                        DispatchQueue.main.async{
                        LocalStorageManager.addMessageToLocalStorage(message: self)
                        print("message is in green text now. saving to local")
                        }
                    }
                    if(self.timer?.remainingIterations == 1){
                        print("message is in blue text now. timer is going ")
                    }
                })
                
            }
        }
    }
    
    override var hashValue: Int {
        return self.timestamp.hash
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        guard let otherMessage = object as? Message else {
            return false
        }
        return self.messageText == otherMessage.messageText &&
            self.username == otherMessage.username &&
            self.timestamp == otherMessage.timestamp
    }
    
    
    private enum CodingKeys: String, CodingKey {
        case username = "username"
        case messageText = "messageText"
        case timestamp = "timestamp"
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        username = try values.decode(String.self, forKey: .username)
        messageText = try values.decode(String.self, forKey: .messageText)
        timestamp = try values.decode(String.self, forKey: .timestamp)
        timer = nil
        isNewMessage = false
    }
    
    public func encode(to encoder: Encoder) throws {
        var values = encoder.container(keyedBy: CodingKeys.self)
        try values.encode(username, forKey: .username)
        try values.encode(messageText, forKey: .messageText)
        try values.encode(timestamp, forKey: .timestamp)
    }
    
    
    
//    public static func == (lhs: Message, rhs: Message) -> Bool {
//        if (lhs.messageText == rhs.messageText &&
//           lhs.username == rhs.username &&
//            lhs.timestamp == rhs.timestamp &&
//            lhs.hashValue == rhs.hashValue )
//            {
//            return true
//                }
//        return false
//    }
    
    private enum PropertyKey: String {
        case name, text, timestamp
    }
    
    var username: String
    var messageText: String
    var timestamp: String
    
    init(json: JSON) {
        self.username = json[PropertyKey.name.rawValue].stringValue
        self.messageText = json[PropertyKey.text.rawValue].stringValue
        self.timestamp = json[PropertyKey.timestamp.rawValue].stringValue
        isNewMessage = false
    }
    
    init(text: String) {
        self.username = ""
        self.messageText = text
        self.timestamp = ""
        isNewMessage = false
    }
   
    override init() {
        self.username = ""
        self.messageText = ""
        self.timestamp = ""
        isNewMessage = false
    }
    
    deinit {
        timer = nil
        cellState.bag.dispose()
    }
}
