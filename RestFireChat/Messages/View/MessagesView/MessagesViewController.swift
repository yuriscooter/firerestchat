//
//  MessagesViewController.swift
//  RestFireChat
//
//  Created by Yuri Che on 9/5/18.
//  Copyright © 2018 Yuri Che. All rights reserved.
//

import UIKit
import Repeat
import Bond
import ReactiveKit

class MessagesViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    
    lazy var messagesViewModel: MessagesViewModel = {
        return MessagesViewModel()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.barTintColor = UIColor(red:0/255, green:0/255, blue:0/255, alpha:1.0)
        Bundle.main.loadNibNamed("MessagesViewController", owner: self, options: nil)
        self.tableView.register(MessageTableViewCell.register(), forCellReuseIdentifier: Keys.messageCellName)
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.rowHeight = 40
        self.tableView.tableFooterView = UIView(frame: .zero)
        addNavBarButton()
        bindTableView()
        messagesViewModel.getLocalMessages()
        _ = messagesViewModel.messagesArray.observeNext { (newEvent) in
            if( newEvent.change != .reset  && !newEvent.dataSource.isEmpty ){
            self.tableView.scrollToRow(at: IndexPath(row: self.messagesViewModel.messagesArray.count - 1, section: 0), at: UITableViewScrollPosition.bottom, animated: true)
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {
            self.messagesViewModel.getMessages()
        })
        print("messages in VC: ", messagesViewModel.messagesArray.count)
    }
    
    func bindTableView() {
        messagesViewModel.messagesArray.bind(to: tableView) { dataSource, indexPath, tableView in
            let cell = tableView.dequeueReusableCell(withIdentifier: Keys.messageCellName, for: indexPath) as! MessageTableViewCell
            cell.reactive.bag.dispose()
            let cellMessage = dataSource[indexPath.row]
            cell.messageForCell = cellMessage
            return cell
            }
    }
    
    func addNavBarButton()  {
        let navButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.compose, target: self, action: #selector( showComposeMessage ))
        navButton.tintColor = UIColor.green
        self.navigationItem.rightBarButtonItem = navButton
    }
    
    @objc func showComposeMessage() {

        self.present(ComposeViewController(), animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
