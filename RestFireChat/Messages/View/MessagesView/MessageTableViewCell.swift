//
//  MessageTableViewCell.swift
//  RestFireChat
//
//  Created by Yuri Che on 9/5/18.
//  Copyright © 2018 Yuri Che. All rights reserved.
//

import UIKit

class MessageTableViewCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    var messageForCell: Message = Message() {
        didSet{
                self.label.text = self.messageForCell.messageText
        _ = self.messageForCell.cellState.observe { (newEvent) in
            DispatchQueue.main.async {
                print("new timer value: ", newEvent.element)
                switch ( newEvent.element ){
                case 0: self.label.textColor = UIColor.green
                case 1: self.label.textColor = UIColor.blue
                case 2: self.label.textColor = UIColor.red
                case .none:
                    self.label.textColor = UIColor.green
                case .some(_):
                    self.label.textColor = UIColor.green
                }
            }
        }
    }
}
    
    static func register() -> UINib {
        return UINib(nibName: "MessageTableViewCell", bundle: nil)
    }
    
    override func prepareForReuse() {
        messageForCell = Message()
        label.text = ""
        //label.textColor = UIColor.white
        
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
