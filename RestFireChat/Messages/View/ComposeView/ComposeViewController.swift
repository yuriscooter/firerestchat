//
//  ComposeViewController.swift
//  RestFireChat
//
//  Created by Yuri Che on 9/6/18.
//  Copyright © 2018 Yuri Che. All rights reserved.
//

import UIKit

class ComposeViewController: UIViewController, UITextViewDelegate {
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var sendButton: UIButton!
    
    lazy var composeViewModel: ComposeViewModel = {
        return ComposeViewModel()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sendButton.addTarget(self, action: #selector(sendMessage), for: UIControlEvents.touchUpInside)
        sendButton.layer.cornerRadius = 5.0
        textView.layer.cornerRadius = 5.0
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        sendButton.isEnabled = !text.isEmpty
        if( !text.isEmpty && text == "\n") {
            sendMessage()
            return false
        }
        return true
    }
    
    @objc func sendMessage(){
        if !textView.text.isEmpty {
            composeViewModel.postMessage(messageText: textView.text) { messageId in
                if(messageId != nil && !((messageId?.isEmpty)!) ){
                self.textView.text = ""
                self.textView.resignFirstResponder()
                self.dismiss(animated: true, completion: nil)
                }
            }
            
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        textView.becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
