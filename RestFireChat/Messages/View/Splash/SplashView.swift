//
//  SplashView.swift
//  MVVM Boilerplate
//
//  Created by Agus Cahyono on 27/11/17.
//  Copyright © 2017 Agus Cahyono. All rights reserved.
//

import UIKit

class SplashView: UIViewController {
    
    @IBOutlet weak var progress: UIProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            let home = UINavigationController(rootViewController: MessagesViewController())
            self.present(home, animated: true, completion: nil)
        }
        
    }


}
