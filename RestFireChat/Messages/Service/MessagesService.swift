//
//  MessagesService.swift
//  RestFireChat
//
//  Created by Yuri Che on 9/4/18.
//  Copyright © 2018 Yuri Che. All rights reserved.
//

import Foundation
import SwiftyJSON


protocol MessagesServiceProtocol {
    func fetchMessages(complete: @escaping (_ messages: [Message]?, _ error: APIError? )->() )
    func sendMessage( message: Message, complete: @escaping ( _ messageID: String?, _ success: Bool? )->()) 
}


class MessagesService: MessagesServiceProtocol {

    func fetchMessages(complete: @escaping ([Message]?, APIError?) -> ()) {
        APIManager.fetchData(url: APIService.baseUrl, method: .get) { (jsonResponse, success) in
            if (success == true && jsonResponse != nil ){
                if let rootJSON = jsonResponse?.dictionaryValue.values {
                    let messagesParsed = rootJSON.map( { Message(json:$0) })
                print("messages amount: ", messagesParsed.count)
                complete(messagesParsed, nil)
                }
            }
            else{
                complete(nil, APIError.networkError)
            }
        }
    }
    
    func sendMessage( message: Message, complete: @escaping ( _ messageID: String?, _ success: Bool? )->()) {
        APIManager.sendMessage(message: message) { ( messageID , completed) in
            if( completed! && messageID != nil ){
                print("post successfull with id: ", messageID! )
                complete( messageID, true )
            }
            else{
                complete( nil, false)
            }
        }
        
    }
    
}













