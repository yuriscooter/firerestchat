//
//  Keys.swift
//  RestFireChat
//
//  Created by Yuri Che on 9/4/18.
//  Copyright © 2018 Yuri Che. All rights reserved.
//

import Foundation

public struct Keys {

    static let nameKey = "name"
    static let defaultsKey = "messages"
    static let messageCellName = "messageCell"
}



