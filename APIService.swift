//
//  APICommunication.swift
//  RestFireChat
//
//  Created by Yuri Che on 9/4/18.
//  Copyright © 2018 Yuri Che. All rights reserved.
//

import Foundation
import Alamofire


enum APIError: String, Error {
    case networkError = "No internet connection"
    case serverError = "Server returned error"
    case notAuthorizedError = "Not authorized"
}

class APIService {
    static let baseUrl = "https://restfirechat.firebaseio.com/messages.json"
}

