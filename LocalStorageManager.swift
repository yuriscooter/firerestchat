//
//  LocalStorageManager.swift
//  RestFireChat
//
//  Created by Yuri Che on 9/5/18.
//  Copyright © 2018 Yuri Che. All rights reserved.
//

import Foundation

class LocalStorageManager {
    
  static func saveToUserdefaults( allMessages: [Message] ){
    var storedMessages = allMessages
    if(LocalStorageManager.readSaved() != nil ){
        storedMessages.append(contentsOf: LocalStorageManager.readSaved()!)
    }
    print("all messages count: ", storedMessages.count)
    let uniqueValues = Set(storedMessages)
    print("unique values count: ", uniqueValues.count)
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(uniqueValues) {
            UserDefaults.standard.set(encoded, forKey: Keys.defaultsKey)
        }
    }
    
  static func readSaved() -> [Message]? {
        if let userData = UserDefaults.standard.data(forKey: Keys.defaultsKey),
            let messagesArray = try? JSONDecoder().decode(Array<Message>.self, from: userData) {
            let uniqueValuesSet = Set(messagesArray)
            let uniqueArray = Array(uniqueValuesSet)
            let sortedArray = uniqueArray.sorted(by: { ( messageOne , messageTwo) -> Bool in
                Timestamp.convertFromTimestamp(seconds: messageOne.timestamp).compare(Timestamp.convertFromTimestamp(seconds: messageTwo.timestamp) as Date) == .orderedAscending
                
            })
            return sortedArray
        }
        return nil
    }
    
   static func addMessageToLocalStorage( message: Message ) {
    if var savedMessages = LocalStorageManager.readSaved(){
            if( LocalStorageManager.isMessageNew(message: message) ){
                savedMessages.append(message)
                LocalStorageManager.saveToUserdefaults(allMessages: savedMessages)
            }
        }
        else{
            var messagesArray = [Message]()
            messagesArray.append(message)
            LocalStorageManager.saveToUserdefaults(allMessages: messagesArray)
    }
}
    
    
    static func isMessageNew(message: Message) -> Bool {
        if( LocalStorageManager.readSaved() == nil ||
            LocalStorageManager.readSaved()?.count == 0 ||
            !(LocalStorageManager.readSaved()?.contains(message))! ){
            return true
        }
        return false
    }
}

