//
//  APIManager.swift
//  RestFireChat
//
//  Created by Yuri Che on 9/4/18.
//  Copyright © 2018 Yuri Che. All rights reserved.
//

import Foundation
import Alamofire
import Alamofire_SwiftyJSON
import SwiftyJSON


class APIManager {
    
    static var manager: SessionManager!
    
    static func fetchData(
        
        url: String,
        method : HTTPMethod,
        headers: HTTPHeaders = HTTPHeaders(),
        parameters: Parameters = Parameters(),
    
        complete: @escaping ( _ response: JSON?, _ success: Bool? )->()) {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 5
        configuration.timeoutIntervalForResource = 5
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        APIManager.manager = Alamofire.SessionManager(configuration: configuration)
        
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default)
            .responseSwiftyJSON { dataResponse in
                if(dataResponse.result.isSuccess) {
                    if let JSON = dataResponse.result.value {
                        print(JSON)
                        complete(JSON, true)
                    }
                }
                if ( dataResponse.error != nil ){
                    print("Error: ", dataResponse.error!.localizedDescription)
                }
        }
    }
    
    static func sendMessage(
        message: Message ,
        headers: HTTPHeaders = ["Accept" : "text/event-stream"] ,
        complete: @escaping ( _ messageID: String?, _ success: Bool? )->()) {
        let parameters = ["user": message.username, "text" : message.messageText, "timestamp": message.timestamp ]
        Alamofire.request(APIService.baseUrl, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseSwiftyJSON
            { dataResponse in
                if(dataResponse.result.isSuccess) {

                    if let jsonResponse = dataResponse.result.value {
                        let messageID = jsonResponse[Keys.nameKey].stringValue
                        print(messageID )
                        complete(messageID, true)
                    }
                }
                
                if ( dataResponse.error != nil ){
                    print("Error: ", dataResponse.error!.localizedDescription)
                }
        }
        
    }

}








